FROM postgres:13

LABEL maintainer="Perita Soft - https://www.peritasoft.com/"

RUN apt-get update \
      && apt-cache showpkg postgresql-$PG_MAJOR-pgtap \
      && apt-get install -y --no-install-recommends \
           postgresql-$PG_MAJOR-pgtap \
      && rm -rf /var/lib/apt/lists/*
